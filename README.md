# AvaloniaExample-Images

This is a demo showing how one can save and load images from a database, and show them in an application, using EntityFramework and Avalonia.

The app in question is simple - it features a single textbox into which the user enters a path to an image.

Upon hitting the "Load" button, the image is both shown on screen, and also saved into the DB.

On subsequent openings of the application, it will automatically load the most recently saved image.

To run this example code, first apply the migrations to create the local DB, then run it and load some images.

Much of the work is being done in the ImageUIViewModel.cs file.

The key idea:
Images must be converted to a byte[] to be stored using Entity Framework
Images must be converted back from a byte[] to a Bitmap to be shown using Avalonia

If you have any questions or feedback about this example, don't hestitate to send a message Andrew!