//Avalonia.Media.Imaging contains the relevant Bitmap class
using Avalonia.Media.Imaging;
using ImageSaver.Models;
using ImageSaver.Services;
using ReactiveUI;
using System.Reactive;


namespace ImageSaver.ViewModels
{
    public class ImageUIViewModel : ViewModelBase
    {
        private string _imagePath;

        //A Textbox control in the UI allows the user to specify a path to a file.
        public string ImagePath{
            get => _imagePath;
            set => this.RaiseAndSetIfChanged(ref _imagePath, value);
        }

        private Bitmap? _displayedImage;

        //The Image Control has a data binding to DisplayedImage
        //when we change the image, the UI is updated
        public Bitmap? DisplayedImage
        {
            get => _displayedImage;
            set{
                //First, update the image being displayed.
                this.RaiseAndSetIfChanged(ref _displayedImage, value);
                //Then save it to the db
                SaveImageToDB(_displayedImage);
                
            }
            
        }
        public ReactiveCommand<Unit, Unit> Load { get; }

        

        public ImageUIViewModel(){
            //The load button loads the image at the specified path.
            Load = ReactiveCommand.Create(() => {DisplayedImage = new Bitmap(ImagePath);});

            //Run a query so the app starts up showing the most recently loaded image
            Bitmap image = LoadImageFromDB();
            if (image != null){
                DisplayedImage = image;
            }
        }

        public void SaveImageToDB(Bitmap image){
            //To save image to DB, we first have to convert it to a byte[]
            var ms = new MemoryStream();
            image.Save(ms);
            byte[] imageBytes = ms.ToArray();

            //Create an EF entity by passing the image byte[]
            //(Note, in this case we are creating a new entity, 
            //if you want to modify the image you would do so by changing
            // the byte[] of an existing entity)
            LoadedImage dbImage = new LoadedImage(imageBytes);

            //Add & Save the image to the dbcontext            
            ImageContext.Instance.Images.Add(dbImage);
            ImageContext.Instance.SaveChanges();
        }

        //Method that loads the most recently uploaded image
        public Bitmap LoadImageFromDB(){
            //Start by running a query to find the most recent image
            LoadedImage image = ImageContext.Instance.Images.OrderByDescending(e => e.TimeLoaded).FirstOrDefault();
            
            //Next, convert the byte[] into a Bitmap
            var ms = new MemoryStream(image.Image);
            return Bitmap.DecodeToWidth(ms, 300);
        }
    }

}
