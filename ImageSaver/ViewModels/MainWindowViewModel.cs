﻿using System;
using System.Reactive.Linq;
using ReactiveUI;

namespace ImageSaver.ViewModels
{
    class MainWindowViewModel : ViewModelBase
    {
        ViewModelBase content;

        public ViewModelBase Content
        {
            get => content;
            private set => this.RaiseAndSetIfChanged(ref content, value);
        }


        public MainWindowViewModel()
        {

            ImageUIViewModel vm = new ImageUIViewModel();
            Content = vm;
        }
    }
}