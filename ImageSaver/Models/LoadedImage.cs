
using Avalonia.Media.Imaging;

namespace ImageSaver.Models;

public class LoadedImage{
    public byte[] Image {get; set;}
    public DateTime TimeLoaded {get; set;}
    public int Id {get; private set;}

    public LoadedImage(byte[] image){
        Image = image;
        TimeLoaded = DateTime.Now;
    }

    private LoadedImage(){

    }
}