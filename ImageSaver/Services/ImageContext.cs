using Microsoft.EntityFrameworkCore;
namespace ImageSaver.Services;
using System;
using System.Collections.Generic;
using ImageSaver.Models;

public class ImageContext : DbContext
{

  public DbSet<LoadedImage> Images {get;set;}

  public string DbPath { get; }

  //Demo uses SQLite Db.
  public ImageContext(){
    var folder = Environment.SpecialFolder.LocalApplicationData;
    var path = Environment.GetFolderPath(folder);
    DbPath = System.IO.Path.Join(path, "images.db");
  }

  private static ImageContext _instance;
  public static ImageContext Instance{
    get {
      if (_instance == null){
        _instance = new ImageContext();
      }
      return _instance;
    }
  }

  protected override void OnConfiguring(DbContextOptionsBuilder options) 
    => options.UseSqlite($"Data Source={DbPath}");



}


